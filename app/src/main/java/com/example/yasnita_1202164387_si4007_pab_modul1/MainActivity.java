package com.example.yasnita_1202164387_si4007_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

//Nama: Yasnita
//NIM: 1202164387
//Kelas: SI4007

public class MainActivity extends AppCompatActivity {
    EditText input1,input2;
    TextView output;
    Double angka1,angka2,hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        input1 = (EditText) findViewById(R.id.inputpertama);
        input2 = (EditText) findViewById(R.id.inputkedua);
        output = (TextView) findViewById(R.id.hasil);
    }
    public void konversi(){
        angka1 = Double.parseDouble(input1.getText().toString());
        angka2 = Double.parseDouble(input2.getText().toString());
    }

    public void hasilperhitungan(View view) {
        konversi();
        hasil = angka1*angka2;
        output.setText(Double.toString(hasil));
    }
}

